#!/usr/bin/python3

#
#   update_camera_fps.py - Updates camera FPS
#       This is the initial EXAMPLE of how to build a python app to act
#           as a 'command' that executes on request from the GUI
#

from enum import Enum
import configparser
import argparse
import json
import collections
import time
import datetime
from datetime import timedelta, timezone
import pprint as pprint
import logging
import pika
import random
import requests

import cmdutils as cmdutils

# This disables the SSL certificate warning
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

JSON_HEADER = {'content-type': 'application/json', 'Accept-Charset': 'UTF-8'}
TEXT_HEADER = {'Content-type': 'text/plain', "Accept": "*/*"}

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--correlationid', '-c', required=True, help='Script correlation id')
    return parser.parse_args()

def set_camera_fps(ip, fps):
    url = 'http://' + ip + "/cgi-bin/admin/setparam.cgi?videoin_c0_s0_h264_maxframe=" + fps
    logging.info("url is {}".format(url))
    requests.get(url, verify=False, headers=TEXT_HEADER, timeout=5)

if __name__ == '__main__':

    args = parse_args()

    logging.basicConfig(filename='/var/log/cpr/update_cameras.log', level=logging.INFO,
                        format='%(asctime)s.%(msecs)03d %(levelname)s %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')

    logging.info("Starting update_camera_fps script...")
    logging.info(args)
    logging.info("Correlation ID is {}".format(args.correlationid))

    # read our command 'state' file
    # { 'userParameters': {'FPS': '2-FPS'}, 'locomotive': '8888'}
    state_filename = '/home/cp_user/commands/data/' + args.correlationid
    state = cmdutils.load_state(state_filename)
    logging.info("State is {}".format(str(state)))

    if 'userParameters' in state: 
        params = state['userParameters']
    else:
        logging.exception("No Parameters sent")
        exit(1)

    if 'FPS' in params:
        fps = params['FPS']
    else:
        logging.exception("No Parameters sent")
        exit(1)

    # Currently just four options coming in, 2,4,8,12 FPS
    camera_fps = '12'
    if '2-FPS' == fps:
        camera_fps = '2'
    elif '4-FPS' == fps:
        camera_fps = '4'
    elif '8-FPS' == fps:
        camera_fps = '8'

    cameras = ['2.2.2.30','2.2.2.31','2.2.2.32','2.2.2.33']
    msg = ""
    for index, camera in enumerate(cameras):
        try:
            msg = msg + " camera: " + camera + "->"
            set_camera_fps(camera, camera_fps)
            msg = msg + 'successful.'
        except Exception as e:
            logging.exception("Exception on camera {} is {}".format(camera,str(e)))
            msg = msg + 'failed.'

    syslog_file = args.correlationid + "syslog.log"

    cmdutils.ack_command(args.correlationid, msg, syslog_file)



