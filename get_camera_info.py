#!/usr/bin/python3

#
#   get_camera_info.py - Basic command script to get some camera info
#

import configparser
import argparse
import requests
import json
import time
import datetime
import logging
import pprint

# This disables the SSL certificate warning
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

JSON_HEADER = {'content-type': 'application/json', 'Accept-Charset': 'UTF-8'}
TEXT_HEADER = {'Content-type': 'application/text', "Accept": "*/*"}

SAVE_DIR = "/home/cp_user/rsync/out_files/"

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--correlationid', '-c', required=True, help='Script correlation id')
    return parser.parse_args()

def get_locomotive(filename):
    loco = "cp.unknown"
    try:
        parser = configparser.ConfigParser()
        parser.read(filename)
        locodict = dict(parser.items('Main'))
        loco = locodict['locomotive']
    except Exception as e:
        logging.exception("get_loco: {0}".format(str(e)))
    return loco

# curl http://2.2.2.30/cgi-bin/admin/getparam.cgi?videoin_flip
# curl http://2.2.2.30/cgi-bin/admin/getparam.cgi?videoin_mirror
# curl http://2.2.2.30/cgi-bin/admin/getparam.cgi?videoin_c0_s0_resolution
# curl http://2.2.2.30/cgi-bin/admin/getparam.cgi?videoin_c0_s0_h264_maxframe
# curl http://2.2.2.30/cgi-bin/admin/getparam.cgi?videoin_whitebalance
# curl http://2.2.2.33/cgi-bin/admin/getparam.cgi?system_info_modelname
# curl http://2.2.2.33/cgi-bin/admin/getparam.cgi?audioin_c0_mute
# curl http://2.2.2.33/cgi-bin/admin/getparam.cgi?audioin_c0_source
# curl http://2.2.2.33/cgi-bin/admin/getparam.cgi?audioin_c0_input
# curl http://2.2.2.33/cgi-bin/admin/getparam.cgi?audioin_c0_boostmic
# curl http://2.2.2.33/cgi-bin/admin/getparam.cgi?audioin_c0_gain

def get_camera_details(ip):
    camera_config = {}
    base_url = 'http://' + ip + '/cgi-bin/admin/getparam.cgi?'
    items = [
            'videoin_flip',
            'videoin_mirror',
            'videoin_c0_s0_resolution',
            'videoin_c0_s0_h264_maxframe',
            'videoin_whitebalance',
            'system_info_modulename',
            'audioin_c0_mute',
            'audioin_c0_source',
            'audioin_c0_input',
            'audioin_c0_boostmic',
            'audioin_c0_gain'
            ]

    for item in items:
        url = base_url + item
        r = requests.get(url, verify=False, headers=TEXT_HEADER)
        result = r.text.rstrip(' \n\r')
        eq = result.find("=")
        if not eq == -1:
            result = result[eq + 2:len(result) - 1]
            print(result)
        camera_config[item] = result

    return camera_config

if __name__ == '__main__':

    args = parse_args()

    logging.basicConfig(filename='/var/log/cpr/get_camera_info.log', level=logging.INFO,
                        format='%(asctime)s.%(msecs)03d %(levelname)s %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')

    locomotive = get_locomotive('/home/cp_user/locomotive.ini')
    logging.info("Starting script...")
    logging.info(args)
    logging.info("Correlation ID is {}".format(args.correlationid))
    logging.info("Locomotive is {}".format(str(locomotive)))


    cameras = ['2.2.2.30','2.2.2.31','2.2.2.32','2.2.2.33']

    state = {}
    state['locomotive'] = locomotive
    state['mongo_key'] = 'CAMERA_INFO'
    state["script_version"] = 1
    state["last_update_date"] = datetime.datetime.utcnow()
    state["correlationid"] = str(args.correlationid)
    state["name"] = "Camera_Configuration_Report"
    state["cameras"] = cameras

    for index, camera in enumerate(cameras):
        msg = {}
        try:
            result = get_camera_details(camera)
            msg[camera] = result
            state["cameras"][index] = msg
        except Exception as e:
            logging.exception("Exception on camera {} is {}".format(camera,str(e)))
            # msg[camera] = getattr(e,'message', str(e))
            msg[camera] = "not found"
            state["cameras"][index] = msg

    logging.info(state)
    pprint.pprint(state)

    # Our task is to write a file to the out_files directory
    try:
        # json - saving
        dt = datetime.datetime.utcnow()
        timestr = dt.strftime("%Y-%m-%d-%H-%M-%S")
        # filename = SAVE_DIR + timestr + '-' + locomotive + "-" + filename + "-" + type + ".json"
        fullpath = SAVE_DIR + timestr + '-' + locomotive + '-CAMERA-INFO-' + args.correlationid + ".json"
        with open(fullpath, 'w') as f:
            json.dump(state, f, indent=4, sort_keys=True, default=str)
    except Exception as e:
        logging.exception("Unable to write file: {}".format(str(e)))
