#!/usr/bin/python3

#
#   reboot_device.py - Reboot a device on the ACC
#

from enum import Enum
import configparser
import argparse
import json
import collections
import time
import datetime
from datetime import timedelta, timezone
import pprint as pprint
import logging
import pika
import random
import subprocess

import cmdutils as cmdutils

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--correlationid', '-c', required=True, help='Script correlation id')
    return parser.parse_args()

if __name__ == '__main__':

    args = parse_args()

    logging.basicConfig(filename='/var/log/cpr/reboot_device.log', level=logging.INFO,
                        format='%(asctime)s.%(msecs)03d %(levelname)s %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')

    logging.info("Starting reboot_device script...")
    logging.info(args)
    logging.info("Correlation ID is {}".format(args.correlationid))

    # Make sure locomotive is in safe_state
    safe = cmdutils.get_safe_state()
    if not safe:
        cmdutils.ack_command(args.correlationid, "Locomotive is not in a SAFE state", "None")
        exit(0)

    # read our command 'state' file
    # { 'userParameters': {'device': 'SMM'}, 'locomotive': 'CP0103'}
    state_filename = '/home/cp_user/commands/data/' + args.correlationid
    state = cmdutils.load_state(state_filename)
    logging.info("State is {}".format(str(state)))

    if 'userParameters' in state: 
        params = state['userParameters']
    else:
        logging.exception("No Parameters sent")
        cmdutils.ack_command(args.correlationid, "No Parameters sent", "None")
        exit(1)

    if 'Device' in params:
        device = params['Device']
    else:
        logging.exception("No Device Parameters sent")
        cmdutils.ack_command(args.correlationid, "No Device Parameters sent", "None")
        exit(1)

    if device == 'SMM':
        addr = 'root@10.255.255.253'
    elif device == 'WCM':
        addr = 'root@10.255.255.254'
    elif device == 'HPEAP':
        addr = 'root@10.255.255.219'
    elif device == 'Sentinel':
        addr = 'root@10.255.255.230'
    else:
        logging.exception("Improper device name provided {}".format(device))
        cmdutils.ack_command(args.correlationid, "Improper device name provided", "None")
        exit(1)

    cmd = []
    cmd.append("/usr/bin/rsh")
    cmd.append(addr)
    cmd.append('reboot')
    logging.info("----> COMMAND {}".format(cmd))
    try:
        proc = subprocess.run(cmd, stdout=subprocess.PIPE, universal_newlines=True)
        logging.info(proc.stdout)

        # process = subprocess.Popen(args, preexec_fn=demote(user_uid, user_gid), cmd=cmd)
        # subprocess.run(['su', 'cp_user'], stdout=subprocess.PIPE, universal_newlines=True)
        # subprocess.run(['whoami'], stdout=subprocess.PIPE)        
        # output = subprocess.check_output(cmd)
    except Exception as e:
        logging.exception("Error during reboot script execution: {}".format(str(e)))

    msg = "Device Rebooted"
 
    cmdutils.ack_command(args.correlationid, msg, "None")

