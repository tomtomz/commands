#!/usr/bin/python3

#
#   get_smm_log.py - Basic command script
#

from enum import Enum
import configparser
import argparse
import json
import collections
import time
import datetime
from datetime import timedelta, timezone
import pprint as pprint
import logging
import pika
import random

import cmdutils as cmdutils

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--correlationid', '-c', required=True, help='Script correlation id')
    return parser.parse_args()

if __name__ == '__main__':

    args = parse_args()

    logging.basicConfig(filename='/var/log/cpr/get_smm_log.log', level=logging.INFO,
                        format='%(asctime)s.%(msecs)03d %(levelname)s %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')

    logging.info("Starting get_smm_log script...")
    logging.info(args)
    logging.info("Correlation ID is {}".format(args.correlationid))


    # parameters dictionary 
    params = cmdutils.get_parameters(args.correlationid)
    logging.info("Parameters blob: {}".format(str(params)))

    # Simulate getting log file
    time.sleep(10)

    syslog_file = args.correlationid + "-syslog.log"
    msg = "SMM log retrieved"
 
    cmdutils.ack_command(args.correlationid, msg, syslog_file)

# To write to a file for RSYNC back if we go that route for some data, etc.
    # fullpath = '/home/cp_user/rsync/out_files/get_smm_log-' + args.correlationid + ".json"
    # try:
    #     with open(fullpath, 'w') as f:
    #         json.dump(result_msg, f, indent=4, sort_keys=True, default=str)
    # except Exception as e:
    #     logging.exception("Unable to write file: {}".format(str(e)))