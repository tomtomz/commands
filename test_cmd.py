#!/usr/bin/python3

#
#   test_cmd.py - Test command that does require a context object
#

from enum import Enum
import configparser
import argparse
import json
import collections
import time
import datetime
from datetime import timedelta, timezone
import pprint as pprint
import logging
import pika
import random
import subprocess
import os

import cmdutils as cmdutils

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--correlationid', '-c', required=True, help='Script correlation id')
    return parser.parse_args()

# def mai(my_args=None):
#     if my_args is None: my_args = sys.argv[1:]
#     user_name, cwd = my_args[:2]
#     args = my_args[2:]
#     pw_record = pwd.getpwnam(user_name)
#     user_name      = pw_record.pw_name
#     user_home_dir  = pw_record.pw_dir
#     user_uid       = pw_record.pw_uid
#     user_gid       = pw_record.pw_gid
#     env = os.environ.copy()
#     env[ 'HOME'     ]  = user_home_dir
#     env[ 'LOGNAME'  ]  = user_name
#     env[ 'PWD'      ]  = cwd
#     env[ 'USER'     ]  = user_name
#     report_ids('starting ' + str(args))
#     process = subprocess.Popen(
#         args, preexec_fn=demote(user_uid, user_gid), cwd=cwd, env=env
#     )
#     result = process.wait()
#     report_ids('finished ' + str(args))
#     print 'result', result


# def demote(user_uid, user_gid):
#     os.setgid(user_gid)
#     os.setuid(user_uid)

# """Check who is running this script"""
def check_username():
    logging.info("who is running: {} {}".format(os.get, os.getguid()))

def check_id():
	"""Run the command 'id' in a subprocess, return the result"""
	cmd = ['id']
	return subprocess.check_output(cmd)

def check_id_as_user():
	"""Run the command 'id' in a subprocess as user 1000,
	return the result"""

	cmd = ['id']
	return subprocess.check_output(cmd, preexec_fn=demote(1000, 1000))

	# """Pass the function 'set_ids' to preexec_fn, rather than just calling
	# setuid and setgid. This will change the ids for that subprocess only"""
def demote(user_uid, user_gid):
    def set_ids():
        os.setgid(user_gid)
        os.setuid(user_uid)

    return set_ids


if __name__ == '__main__':

    args = parse_args()

    logging.basicConfig(filename='/var/log/cpr/test_cmd.log', level=logging.INFO,
                        format='%(asctime)s.%(msecs)03d %(levelname)s %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')

    logging.info("Starting test script...")
    logging.info(args)
    logging.info("Correlation ID is {}".format(args.correlationid))

    # check_username()

    # logging.info("ID is {}".format(check_id()))
    # logging.info("ID as user: {}".format(check_id_as_user()))
    # logging.info("ID is {}".format(check_id()))

    # # read our command 'state' file
    # # { 'userParameters': {'device': 'SMM'}, 'locomotive': 'CP0103'}
    # state_filename = '/home/cp_user/commands/data/' + args.correlationid
    # state = cmdutils.load_state(state_filename)
    # logging.info("State is {}".format(str(state)))

    # if 'userParameters' in state: 
    #     params = state['userParameters']
    # else:
    #     logging.exception("No Parameters sent")
    #     exit(1)

    # if 'Device' in params:
    #     device = params['Device']
    # else:
    #     logging.exception("No Device Parameters sent")
    #     exit(1)

    device = 'SMM'

    if device == 'SMM':
        addr = 'root@10.255.255.253'
    elif device == 'WCM':
        addr = 'root@10.255.255.254'
    elif device == 'HPEAP':
        addr = 'root@10.255.255.219'
    elif device == 'Sentinel':
        addr = 'root@10.255.255.230'
    else:
        logging.exception("Improper device name provided {}".format(device))
        exit(1)

    cmd = []
    cmd.append("/usr/bin/rsh")
    cmd.append(addr)
    cmd.append('reboot')
    logging.info("----> COMMAND {}".format(cmd))
    try:

        # process = subprocess.Popen(args, preexec_fn=demote(user_uid, user_gid), cmd=cmd)
        # proc = subprocess.run(['su', 'cp_user'], stdout=subprocess.PIPE, universal_newlines=True)
        # print(proc.stdout)
        proc = subprocess.run(cmd, stdout=subprocess.PIPE, universal_newlines=True)
        print(proc.stdout)
        # output = subprocess.check_output(cmd)
        # print(output)
    except Exception as e:
        print("Error during test script execution: {}".format(str(e)))
        logging.exception("Error during test script execution: {}".format(str(e)))



