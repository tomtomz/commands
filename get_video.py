#!/usr/bin/python3

#
#   reboot_device.py - Reboot a device on the ACC
#

from enum import Enum
import configparser
import argparse
import json
import collections
import time
import datetime
from datetime import timedelta, timezone
import pprint as pprint
import logging
import pika
import random

HOST = 'main.lsmm.io'
RESULT_QUEUE = 'api.command.results'

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--correlationid', '-c', required=True, help='Script correlation id')
    return parser.parse_args()

def get_locomotive(filename):
    loco = "cp.unknown"
    try:
        parser = configparser.ConfigParser()
        parser.read(filename)
        locodict = dict(parser.items('Main'))
        loco = locodict['locomotive']
    except Exception as e:
        logging.exception("get_loco: {0}".format(str(e)))
    return loco


if __name__ == '__main__':

    args = parse_args()

    logging.basicConfig(filename='/var/log/cpr/get_smm_log.log', level=logging.INFO,
                        format='%(asctime)s.%(msecs)03d %(levelname)s %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')

    logging.info("Starting reboot_device script...")
    logging.info(args)
    logging.info("Correlation ID is {}".format(args.correlationid))

    locomotive = get_locomotive('/home/cp_user/locomotive.ini')

    credentials = pika.PlainCredentials('locomotive', '00Ecco**')
    parameters = pika.ConnectionParameters(host=HOST,
                                    port=5672,
                                    virtual_host='/',
                                    blocked_connection_timeout=5.0,
                                    credentials=credentials)
    connection = pika.BlockingConnection(parameters)
    out_channel = connection.channel()

    # Simulate getting log file
    time.sleep(15)

    filename = args.correlationid + "-video.mvk"

    result_msg = {}
    result_msg['locomotive'] = locomotive
    result_msg['correlationId'] = args.correlationid
    result_msg['status'] = 'Complete'
    result_msg["completeTime"] = datetime.datetime.now(tz=timezone.utc).timestamp()
    result_msg["message"] = "Video retrieved"
    result_msg["filename"] = filename

    result = json.dumps(result_msg, indent=4, sort_keys=True, default=str)
    logging.info("Acknowledging command {}".format(str(result)))
    out_channel.basic_publish(exchange='',
                    routing_key=RESULT_QUEUE,
                    body=result)

# To write to a file for RSYNC back if we go that route for some data, etc.
    # fullpath = '/home/cp_user/rsync/out_files/get_smm_log-' + args.correlationid + ".json"
    # try:
    #     with open(fullpath, 'w') as f:
    #         json.dump(result_msg, f, indent=4, sort_keys=True, default=str)
    # except Exception as e:
    #     logging.exception("Unable to write file: {}".format(str(e)))