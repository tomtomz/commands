#
# cmdutils.py - Utilities common to all command scripts
#

from enum import Enum
import configparser
import argparse
import json
import collections
import time
import datetime
from datetime import timedelta, timezone
import pprint as pprint
import logging
import pika
import random
import requests
from influxdb import InfluxDBClient

logger = logging.getLogger(__name__)

HOST = 'main.lsmm.io'
RESULT_QUEUE = 'api.command.results'

def get_parameters(correlationId):
    try:
        filename = '/home/cp_user/commands/data/' + correlationId
        with open(filename) as f:
            params = json.load(f)
        return params
    except Exception as e:
        logging.exception("get_parameters: {0}".format(str(e)))


def load_state(filename):
    try:
        with open(filename, 'r') as f:
            state = dict(json.load(f))
        return state
    except Exception as e:
        logging.exception("Unable to load state file: {}".format(str(e)))
        return {}

def get_locomotive_id():
    loco = "CP0000"
    try:
        parser = configparser.ConfigParser()
        parser.read('/home/cp_user/locomotive.id')
        locodict = dict(parser.items('Main'))
        loco = locodict['locomotive']
    except Exception as e:
        logging.exception("get_locomotive_id: {0}".format(str(e)))
    logging.debug("Found locomotive ID {}".format(loco))
    return loco

#
#  Also try and get IP from the .id file
# 
def get_locomotive_IP():
    ip = "0.0.0.0"
    try:
        parser = configparser.ConfigParser()
        parser.read('/home/cp_user/locomotive.id')
        locodict = dict(parser.items('Main'))
        ip = locodict['ip']
    except Exception as e:
        logging.exception("get_locomotive_IP: {0}".format(str(e)))
    logging.debug("Found locomotive IP {}".format(ip))
    return ip

def get_datetime():
    # Also works with Z instead of +00:00
    dt = datetime.datetime.utcnow()
    return dt.isoformat("T") + "Z"

def ack_command(corrId, message, filename):
    logging.info("Acknowledging command...")

    try:
        credentials = pika.PlainCredentials('locomotive', '00Ecco**')
        parameters = pika.ConnectionParameters(host=HOST,
                                        port=5672,
                                        virtual_host='/',
                                        blocked_connection_timeout=5.0,
                                        credentials=credentials)
        connection = pika.BlockingConnection(parameters)
        out_channel = connection.channel()

        result_msg = {
            "locomotiveId": get_locomotive_id(),
            "correlationId": corrId,
            "status": 'Complete',
            "message": message,
            "filename": filename,
            "completeTime": get_datetime()
        }
        result = json.dumps(result_msg, indent=4, sort_keys=True, default=str)
        logging.info("Acknowledging command {}".format(str(result)))
        
        out_channel.basic_publish(exchange='',
                        routing_key=RESULT_QUEUE,
                        body=result)
        out_channel.close()
        connection.close()
    except Exception as e:
        logging.exception("ack_command: {}".format(str(e)))

def query_kapacitor(query):
    try:
        influx = InfluxDBClient('localhost', database='kapacitor')
        res = influx.query(query)
    except Exception as e:
        logging.exception("InfluxDB query: {0}".format(str(e)))
        pass
    return res

def get_safe_state():
    safe_state = False
    try:
        result = query_kapacitor('SELECT LAST(*) FROM "Safe_State"')
        if result:
            points = result.get_points()
            for item in points:
                if 'last_value' in item:
                    return item['last_value']
    except Exception as e:
        logging.exception("Kapacitor query: {}".format(str(e)))

    return safe_state
        